package ContinuedSquares;

import ContinuedSquares.controllers.Info;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {

	public static void main(String args[]){
		launch(args);
	}

	@Override
	public void start(Stage stage) throws Exception {

		Parent fxml = FXMLLoader.load(getClass().getClassLoader().getResource("start.fxml"));
		stage.setScene(new Scene(fxml));
		stage.show();

	}
}
