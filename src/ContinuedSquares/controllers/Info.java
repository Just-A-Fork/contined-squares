package ContinuedSquares.controllers;

import com.sun.javafx.scene.traversal.Direction;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

import java.awt.event.ActionEvent;
import java.net.URL;
import java.util.ResourceBundle;

public class Info implements Initializable {

	double number;

	@FXML Canvas squares;
	@FXML Canvas info;

	private final int MAX_HEIGHT = 270,
						MAX_WIDTH = 400,
						PADDING = 25;

	public Info(Stage stage, double number){

		this.number = number;

		FXMLLoader fxml = new FXMLLoader(getClass().getClassLoader().getResource("info.fxml"));
		fxml.setController(this);
		try {
			stage.setScene(new Scene(fxml.load()));
		}catch(Exception e){
			System.err.println("ERROR: Failed to load info");
			e.printStackTrace();
		}
	}

	@Override
	public void initialize(URL url, ResourceBundle resourceBundle) {

		GraphicsContext GSquares = squares.getGraphicsContext2D();
		GraphicsContext GInfo = info.getGraphicsContext2D();

		double height = MAX_HEIGHT,
				width = MAX_WIDTH;

		if(number*MAX_HEIGHT > MAX_WIDTH){
			height = MAX_WIDTH/number;

		}else{
			width = number*MAX_HEIGHT;
		}

		GSquares.strokeRect(PADDING, PADDING, width, height);
		SplitRect(GSquares, Direction.RIGHT, PADDING, PADDING, width, height);


		GInfo.strokeText(String.valueOf(number), PADDING, PADDING, 100);


	}

	static void SplitRect(GraphicsContext g, Direction d, double x, double y, double w, double h){

		double Span = (d == Direction.UP || d == Direction.DOWN)? h : w;
		double SideLen = (d == Direction.UP || d == Direction.DOWN)? w : h;
		int count = (int)Math.floor(Span/SideLen);
		Direction NextDir = Direction.NEXT;
		double NextX = 0, NextY = 0, NextW = 0, NextH = 0;

		switch (d) {
			case RIGHT:
			case NEXT:
				for (int i = 0; i < count; i++) {
					g.strokeRect(x+SideLen*i, y, SideLen, SideLen);
				}
				NextDir = Direction.DOWN;
				NextW = Span%SideLen;
				NextX = x+(Span-NextW);
				NextH = h;
				NextY = y;
				break;

			case DOWN:
				for (int i = 0; i < count; i++) {
					g.strokeRect(x, y+SideLen*i, SideLen, SideLen);
				}
				NextDir = Direction.LEFT;
				NextW = w;
				NextX = x;
				NextH = Span%SideLen;
				NextY = y+(Span-NextH);
				break;

			case LEFT:
			case PREVIOUS:
				for (int i = 1; i <= count; i++) {
					g.strokeRect((x+Span)-SideLen*i, y, SideLen, SideLen);
				}
				NextDir = Direction.UP;
				NextW = Span%SideLen;
				NextX = x;
				NextH = h;
				NextY = y;
				break;

			case UP:
				for (int i = 1; i <= count; i++) {
					g.strokeRect(x, (y+Span)-SideLen*i, SideLen, SideLen);
				}
				NextDir = Direction.RIGHT;
				NextW = w;
				NextX = x;
				NextH = Span%SideLen;
				NextY = y;
				break;
		}

		if(NextH >= 1 && NextW >= 1){
			SplitRect(g, NextDir, NextX, NextY, NextW, NextH);
		}
	}
}
