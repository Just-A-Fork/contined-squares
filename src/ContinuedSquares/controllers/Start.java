package ContinuedSquares.controllers;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

import java.net.URL;
import java.util.ResourceBundle;

public class Start implements Initializable {

	@FXML ComboBox Selection;
	@FXML TextField Custom;
	@FXML Button NextButt;
	@FXML BorderPane root;

	public final double GOLDEN_RATIO = (1+Math.sqrt(5))/2;

	@Override
	public void initialize(URL url, ResourceBundle resourceBundle) {
		Selection.valueProperty().addListener((ob, old_val, new_val)->{
			if(new_val.equals("Custom")){
				Custom.setVisible(true);
			}

			if(!new_val.toString().isEmpty()){
				NextButt.setDisable(false);
			}
		});
	}

	@FXML
	private void NextScene(ActionEvent e){
		Stage stage = (Stage)root.getScene().getWindow();
		double number = 0;

		switch(Selection.getValue().toString()){
			case "Golden Ratio":
				number = GOLDEN_RATIO;
				break;

			case "Euler's Number":
				number = Math.E;
				break;

			case "Pi":
				number = Math.PI;
				break;

			case "Custom":
				number = Double.parseDouble(Custom.getText());
				break;
		}

		new Info(stage, number);

	}
}
